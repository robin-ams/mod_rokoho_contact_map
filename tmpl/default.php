<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!-- Rokoho Contact Map Module -->

<div class="contact-map">
    <script>


    function initMap(div){


		var styles = [

<?php 
if( $params->get('mapstyles')){
    echo $params->get('mapstyles');
}
else {
    echo'
		  {
			stylers: [
			  { hue: "#CCCCCC" },
              { saturation: -90 }
			]
		  },{
            featureType: "road",
            elementType: "all",
            stylers: [
              { hue: "#444444" },
              { weight: 1.5 }
            ]
          },{
            featureType: "administrative",
            elementType: "labels",
            stylers: [
              { hue: "#444444" }
            ]
          },{
			featureType: "poi",
			elementType: "labels",
			stylers: [
			  { visibility: "off" }
			]
		  },{
			featureType: "water",
			stylers: [
			  { hue: "#244573" },
              { saturation: 10 }
			]
		  },{
            featureType: "poi.park",
            stylers: [
              { hue: "00ffff" },
              { saturation: 0 }
    
            ]
          }
';} ?>
		];


        var image = '<?php if($params->get('mapmarker')){echo 'images/markers/' . $params->get('mapmarker');} ?>'
        var locations = [


<?php 
    $c = 0;

foreach ($contact_vars as $key => $carray) :
?>

          ['<?php echo $carray['name'] . "',
          '" . $carray['address'] . ", " . $carray['postcode'] . ", " . $carray['suburb'] . "',
          '" . $carray['image'] . "',
          '" . $carray['address'] . "',
          '" . $carray['postcode'] . "',
          '" . $carray['suburb'] . "',
          '" . $carray['telephone'] . "',
          '" . $carray['mobile'] 

          ;?>'],
<?php
$c++;
endforeach;
?>

        ];








    var infowindow = new google.maps.InfoWindow({maxWidth: 500});
    var geocoder = new google.maps.Geocoder();
    var latlngbounds = new google.maps.LatLngBounds();



    var map = new google.maps.Map(document.getElementById(div), {
      // zoom: 15,
      center: new google.maps.LatLng(0, 0),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    map.setOptions({styles: styles});

    var i;

    for (i = 0; i < locations.length; i++) {
      geocodeAddress(locations[i],latlngbounds);
    }


function geocodeAddress(location,latlngbounds) {
  geocoder.geocode( { 'address': location[1]}, function(results, status) {
  //alert(status);
    if (status == google.maps.GeocoderStatus.OK) {

      //alert(results[0].geometry.location);

      map.setCenter(results[0].geometry.location);
      
      createMarker(
        results[0].geometry.location,
        "<div class=\"infowindow\"><span class=\"logo pull-right\"></span><h4>"+location[0]+"</h4>"+location[3]+"<br>"+location[4]+" "+location[5]+"<br>T:&nbsp;"+location[6]+"&nbsp;M:&nbsp;"+location[7]+"</div>"
        );
      latlngbounds.extend(results[0].geometry.location);
      map.fitBounds(latlngbounds);
      map.setZoom(map.getZoom()<?php echo  $params->get('zoomoffset'); ?>);
    }
    else
    {
      alert("some problem in geocode" + status);
    }
  }); 
}

function createMarker(latlng,html){ 
  var marker = new google.maps.Marker({
    position: latlng,
    icon: image,
    animation: google.maps.Animation.DROP,
    map: map
  }); 



  google.maps.event.addListener(marker, 'click', function() { 
    infowindow.setContent(html);
    infowindow.open(map, marker);
  });
/*    
  google.maps.event.addListener(marker, 'mouseout', function() { 
    infowindow.close();
  });
*/
}

    // map.setCenter(latlngbounds.getCenter());
    //map.fitBounds(latlngbounds);


}

var div = 'div';





        </script>


    <div id="googleMap"></div>
    <div id="mapOverlay">
        <div id="closeMap">
            <i class="fa fa-times fa-2x"></i>
        </div>  
        <div id="googleBigMap"></div>
    </div>
    <a href="#nowhere" id ="bigMap" class="btn btn-warning "><i class="fa fa-search-plus"></i> Zoom map</a>

</div>

<script type="text/javascript">


jQuery(document).ready(function($){

initMap('googleMap');

    $("#bigMap").click(function() {
        
        $('#mapOverlay').addClass('initialized');

        initMap('googleBigMap');
     });
    
    $("#closeMap").click(function() {
        
        $('#mapOverlay').removeClass('initialized');

     });
    

});
  
</script> 
<!-- End Contact Map Module -->