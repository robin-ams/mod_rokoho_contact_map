<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// Include the syndicate functions only once
require_once( dirname(__FILE__).'/helper.php' );

$cid = $params->get('cid');
$contact_vars = modContactMapHelper::getContactDetails($cid);


require( JModuleHelper::getLayoutPath( 'mod_rokoho_contact_map' ) );

?>